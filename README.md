### What is this repository for? ###

## Test task for a PHP developer working on ukfx websites

This is a task which doesn't require you to add any project specific code or any special features. Please use the code which is on http://getbootstrap.com (copy paste)

Add to the repository only source files (adding any libraries/vendors to .gitignore)

Follow your own framework best practices (eg Symfony, Slim). 

The task shouldn't take you more than 30mins unless you prove you needed extra time or had technical problems or you need to include more complicated configuration to present your skills in certain area (eg docker, symfony etc)

Feel free to use any technology to finish this task as long as you can show your experience with PHP.

Not necessary, but you can deliver result as a fully working docker container

If you need to ask, just email me (Peter) with ANY questions

### How do I get set up? ###

* Please fork this repository
* Create new page using your favourite technology
* Add on the page top navigation, few panels with 'Ipsum' texts
* Push code to the repository
* Add simple instructions how to set it up to this README
* Create a pull request for us to review


### Who do I talk to? ###

* Peter piogrek@gmail.com